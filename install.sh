#!/bin/bash

cd /tmp

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
wget --quiet -O - http://nginx.org/keys/nginx_signing.key | apt-key add -


echo "deb http://nginx.org/packages/ubuntu/ xenial nginx" > /etc/apt/sources.list.d/nginx.list
echo "deb-src http://nginx.org/packages/ubuntu/ xenial nginx" >>  /etc/apt/sources.list.d/nginx.list
echo "deb http://download.mono-project.com/repo/ubuntu xenial main" > /etc/apt/sources.list.d/mono-official.list

apt-get update

apt install -y  nginx mono-complete  mono-fastcgi-server4

rm /etc/nginx/conf.d/default.conf
cd /etc/nginx/conf.d
wget https://bitbucket.org/IlyaSosnovskiy/parking/raw/master/parking.conf

mkdir -p /var/www
chown -R www-data /var/www

# uncomment this if you really wont to debug this
# apt install mono-xsp

mkdir -p /etc/mono/fcgi/apps-available
mkdir -p /etc/mono/fcgi/apps-enabled

echo "/:/var/www/parking" > /etc/mono/fcgi/apps-available/parking
ln -s /etc/mono/fcgi/apps-available/parking /etc/mono/fcgi/apps-enabled/parking

echo "# for mono" >> /etc/nginx/fastcgi_params
echo "fastcgi_param  PATH_INFO          \"\";" >> /etc/nginx/fastcgi_params
echo "fastcgi_param  SCRIPT_FILENAME    \$document_root\$fastcgi_script_name;" >> /etc/nginx/fastcgi_params

wget https://bitbucket.org/IlyaSosnovskiy/parking/raw/master/monoserve.sh
cp monoserve.sh /etc/init.d/monoserve
chmod +x /etc/init.d/monoserve
update-rc.d monoserve defaults
rm monoserve.sh

service monoserve stop
service nginx stop

service monoserve start
service nginx start

exit 0




